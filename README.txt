CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The SSL Assistant module assistant to administer SSL certificates.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/ssl

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/ssl

REQUIREMENTS
------------

The module is independent from other modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Customize the SSL settings in Administration » Configuration » System » SSL.
   (/admin/config/system/ssl)

MAINTAINERS
-----------

Current maintainers:
 * azovsky - https://www.drupal.org/user/330533
