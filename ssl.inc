<?php

/**
 * @file
 * SSL Assistant module.
 */

/**
 * Returns administration form for SSL module.
 *
 * @return array
 */
function ssl_admin_form() {
  $form = array();

  $last_check = variable_get('ssl_last_check', 0);
  $last_check_msg = $last_check == 0
    ? t('Check has not been performed yet.')
    : t('Last check: ') . date(SSL_DEF_DATE_FORMAT, $last_check);
  $form['ssl_conf_informer'] = array(
    '#markup' => '<p>' . $last_check_msg . '</p>',
  );

  $options_day = range(0, 32);
  $form['ssl_conf_check_period'] = array(
    '#type' => 'select',
    '#title' => t('Check every'),
    '#options' => $options_day,
    '#default_value' => variable_get('ssl_conf_check_period', 1),
    '#field_suffix' => t('day(s)'),
    '#description' => t('How often to run SSL checking. <em>0</em> value means to disable checking.'),
  );

  $form['ssl_conf_warn_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Warning period'),
    '#default_value' => variable_get('ssl_conf_warn_period', 7),
    '#field_prefix' => t('Shows a warning for'),
    '#field_suffix' => t('day(s) before the certificate expires.'),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['ssl_conf_show_warn'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show warnings in a message for the administrator'),
    '#default_value' => variable_get('ssl_conf_show_warn', 1),
  );
  $form['ssl_conf_email_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Send warning to e-mail(s)'),
    '#default_value' => variable_get('ssl_conf_email_to', ''),
    '#description' => t('To specify multiple recipients, separate each e-mail address with a comma.'),
    // @TODO: Verifie the syntax of the e-mails.
  );

  $form['ssl_conf_email_to_period'] = array(
    '#type' => 'select',
    '#title' => t('Send e-mail(s) period'),
    '#options' => ssl_emails_peiord(),
    '#default_value' => variable_get('ssl_conf_email_to_period', 0),
    '#description' => t('How many times to send warning messages in e-mail.'),
  );


//  $form['#validate'][] = 'ssl_admin_form_validate';

  return system_settings_form($form);
}

//function ssl_admin_form_validate($form, $form_state) {
//  $values = $form_state['values'];
//
//}
